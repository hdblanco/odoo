# -*- coding: utf-8 -*-
#from openerp.osv import osv,fields
import urllib2 as u
import string
import simplejson as json

#class res_currency_rate(osv.osv):
#	_name = "res.currency.rate"
#	_inherit = "res.currency.rate"
"""
{'UFs': [{'Fecha': '2015-02-03', 'Valor': '24.547,63'}]}
{'Euros': [{'Fecha': '2015-02-03', 'Valor': '717,09'}]}
{'Dolares': [{'Fecha': '2015-02-03', 'Valor': '632,19'}]}
{'UTMs': [{'Fecha': '2015-02-01', 'Valor': '43.025'}]}
"""	
#	def currency_schedule_update(self,cr,uid,context=None):
# llamada api, respuesta, moneda a actualizar
indexes = {
    ('dolar','Dolares','USD'),
    ('euro','Euros','EUR'),
    ('uf','UFs','UF'),
    ('utm','UTMs','UTM'),
}
from apikey import apikey

for index in indexes:
    url = 'http://api.sbif.cl/api-sbifv3/recursos_api/'+index[0]+'?apikey='+apikey+'&formato=json'
    f = u.urlopen(url)
    data = f.read()
    data_json = json.loads(data)
    rate = float(data_json[index[1]][0]['Valor'].replace('.','').replace(',','.'))
    
    print index[2],rate
